## How to use this

* You should have **inkscape** installed and added 
  to path in order to complile svg pictures.
  Additionaly you should add `-shell-escape` to the command compilers option, 
  e.g.:
  
    > `pdflatex.exe  -shell-escape -synctex=1 -interaction=nonstopmode  %.tex` 
    or 
    > `xelatex.exe  -shell-escape -synctex=1 -interaction=nonstopmode %.tex`

* Use to `bibtexu` to properly compile bib files with russian names.

* To count publication in authorpapersVAK.bib and authorpapers.bib 
  using **bibtex** and **bibunits** packages you should run `bibtexu` command
  on `bu1.aux` and `bu2.aux` files first.