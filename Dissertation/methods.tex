\section{Основы метода вольт-фарадных измерений}
\label{sec:cap-methods-review}


Вольт-фарадные методы основаны на измерении зависимости емкости исследуемой 
структуры от приложенного напряжения. Эти методы позволяют 
определять различные параметры полупроводников,
в этом разделе акцентируется  внимание только на применении вольт-фарадных 
методов для определении концентрации легирующей примеси или основных носителей 
заряда.


В основе вольт-фарадных методов лежит электронная теория приповерхностной 
области объемного заряда и дифференциальной емкости~\cite{Pavlov1987}.
Ниже представлены основные выражения, которые часто используются в методе 
вольт-фарадного профилирования. 
Эти формулы выводятся в приближении полного обеднения, а концентрация 
определяется на границе области объемного заряда.
Зависимость емкости от напряжения в однородно легированном полупроводнике 
\textit{n}-типа можно записать следующим образом~\cite{sze1984}:

\begin{equation} \label{eq:cap_con}
C = A\sqrt{\frac{q\varepsilon_0\varepsilon N_|D|}{2\left(V_|b|-V-kT/q\right)}}, 
\end{equation}
\noindent где 
$\varepsilon$~--- диэлектрическая проницаемость полупроводника,
$N_|D|$~--- концентрация доноров и
$V_|b|$~--- высота барьера.

\begin{samepage}
Выражение~\eqref{eq:cap_con} можно переписать в виде

\begin{equation} \label{eq:one_over_cap}
\frac{1}{C^2}= \frac{2A^2\left(V_|b|-V-kT/q\right)}{q\varepsilon_0\varepsilon 
N_|D|}.
\end{equation}
\end{samepage}
Продифференцировав уравнение~\eqref{eq:one_over_cap} по напряжению можно найти 
концентрацию доноров

\begin{equation} \label{eq:doping_conc}
N_|D| = \frac{2}{q\varepsilon_0\varepsilon A^2} 
\left[-\frac{1}{d(1/C^2)/dV}\right] = 
\frac{-\,C^3}{q\varepsilon_0\varepsilon A^2}\left(\frac{dC}{dV}\right)^{-1}.
\end{equation}


При этом, емкость обедненного слоя ($C$) напрямую связанна с шириной области 
объемного 
заряда ($x_|d|$)

\begin{equation} \label{eq:cap_xd}
C = \frac{\varepsilon_0\varepsilon A}{x_|d|}.
\end{equation}
\noindent Эта связь позволяет получать профиль концентрации сдвигая границу 
ООЗ за счет изменения напряжения смещения.  

%%%%%%% Стандартный метод измерения емкости

Наиболее популярный метод измерения емкости заключается в использовании 
переменного напряжения малой амплитуды на фоне постоянного напряжения смещения. 
А вольт-фарадные измерения, обычно,  проводят при медленно меняющемся 
смещении.
Барьерная емкости в полупроводниках нелинейно зависит от напряжения,  
поэтому используется понятие дифференциальной емкости~\cite{Berman1968}.
Приложенное переменное напряжение $dV$ индуцирует заряд $dQ$, тогда 
дифференциальная емкость будет определяться как 

\begin{equation}
C = \frac{dQ}{dV}.
\end{equation} 


%\subsubsection{Квазистатический метод}
Другим распространенным методом измерения емкости при проведении вольт-фарадных 
измерений является квазистатический метод.
В этом методе используется то, что ток смещения через конденсатор 
пропорционален изменению напряжения, подаваемого на него:
\begin{equation} \label{eq:cur_cap}
I = \frac{dQ}{dt} = \left( \frac{dQ}{dV}\right) \left(\frac{dV}{dt} \right) =
C \left(\frac{dV}{dt} \right).
\end{equation}

На практике, на исследуемую структуру подается пилообразное напряжение, а сила 
тока
в цепи измеряется через падение напряжения на резисторе, подключенном 
последовательно, или при помощи интегрирующего усилителя (см. 
рис.~\ref{fig:methods_quasistatic}).

\begin{figure}
    \scalebox{0.75}{
    \begin{tikzpicture}[scale=1.5]
    \draw
        (3,0) node[op amp](opamp2) {}
        (1,0.33) node[circ](X){} |- (opamp2.-)
        (opamp2.out) ++ (1,0) node[circ](OUT){} --(opamp2.out)
        (-1,-1.5) node[sground,scale=0.6] {} 
        to [sV={$v(t) \textrm{=} v_0\pm at$}] ++(0,1.82) 
         to [C, l^=$C_|DUT|$]  
        ++(2,0) |- (opamp2.-)
        (opamp2.+) ++(0,-1)  node[sground, scale=0.6] {}
        to (opamp2.+)
        (X)-- ++(0,1) to [R, l^=$R$] ++(3,0) -| (OUT)
        (OUT) --  ++(1,0) node[currarrow, label=right:{~$i(t)\cdot R$}]{}
        ;
    \end{tikzpicture}
    }
    \caption{Пример схемы для измерения квазистатических вольт-фарадных 
    характеристик~\cite{Kuhn1970}.
        \label{fig:methods_quasistatic}}
\end{figure}


Измеренные этим методом вольт-фарадные характеристики можно считать 
квазистатическими если 
образец находится в тепловом равновесии на протяжении всей процедуры измерения.
Т.е. когда неосновные носители заряда и имеющиеся поверхностные состояния 
успевают за изменением переменного зондирующего сигнала и за изменением 
напряжения смещения. Основная сложность в реализации квазистатического метода 
заключается в том, что, при медленной развертке напряжения, токи смещения 
обычно находятся в диапазоне от единиц~\si{\pico\ampere} до 
единиц~\si{\nano\ampere}. А токи такой величины уже сравнимы с токами утечки в 
самой структуре.

%Аналогичные принципы лежат в основе одного из методов реализации 
%нестационарных 
%вольт-фарадных измерений с той лишь разницей, что используется высокая 
%скорость развертки напряжения. 
%Нестационарные методы измерения емкости будут подробнее рассмотрены в 
%разделе~\ref{}.
%%TODO дописать про линейный метод в разделе про нестационарные измерения

%\subsubsection{Импульсный метод}
% 1982 nicollian p. 611

Другие методы измерения вольт-фарадных характеристик,
такие как, например,
использование $LC$-автогенератора~\cite{sergeev2014} или 
различные неравновесные методики~\cite{moseychuk1996,Sakalauskas2011,zhao2014}
редко используются для измерения концентрационных профилей, но позволяют 
получить другие параметры исследуемых структур.

\section{Измерение концентрации в методе вольт-фарадного профилирования}
\label{sec:cv-methods-review}

Интерес к рассмотрению методов измерения и расчета концентрации, которые 
использовались в вольт-фарадных измерениях вызван тем, что многие из этих 
методов применялись в автоматизированных установках для электрохимического 
вольт-фарадного профилирования. Ограничения используемых методов, во многом, 
определяли то, как рассматривались методологические основы метода ECV. Так, 
использование низких, и зачастую строго фиксированных, частот привило к тому, 
что вопрос неполной ионизации примеси и то, как это влияет на результаты ECV 
измерений ранее не рассматривался.

\subsection{Дифференциальный метод измерения концентрации и использование 
номограмм}

Дифференциальный метод определения концентрации из вольт-фарадных характеристик 
заключается в проведении численного дифференцирования  ВФХ построенных в 
координатах Мотта-Шоттки ($1/C^2-V$).
Производную зависимости~\eqref{eq:one_over_cap}, при этом,  находят методом 
конечных приращений~\cite{Pavlov1987}:
\begin{equation}
N\left( x\right) = \frac{2}{q\varepsilon\varepsilon_0 A^2}\
                  \frac{\Delta V}{\Delta\left(1/C^2\right) } = 
                  \frac{2}{q\varepsilon\varepsilon_0 A^2}\
                  \frac{V_2-V_1}{1/C_2^2-1/C_1^2},
\end{equation}
\noindent где $C_1$ -- емкость измеренная при напряжении $V_1$, а $C_2$ -- 
емкость измеренная при напряжении $V_2$ и координата $x = 2 
\varepsilon\varepsilon_0/\left( C_1 + C_2\right) $ соответствует середине 
интервала $\Delta C = C_2 - C_1$.
\nomenclature{$\varepsilon$}{Диэлектрическая проницаемость}

\begin{figure}
    \includesvg[clean,width=0.7\textwidth]{images/methods_nomograph}
    \caption{Номограмма для расчета концентрации примеси в 
             кремнии из вольт-фарадных измерений~\cite{Thomas1962}.
    \label{fig:nomograph}}
\end{figure}

Современные вычислительные средства позволяют без проблем проводить численный 
расчет производных, однако, на момент разработки данной теории 
необходимость выполнять подобные расчеты вручную сильно ограничивало 
распространение метода.
Для того чтобы упростить вычисления, связанные с определением концентрационных 
профилей, M.\,M.~Atalla и D.~Kahng предложили использовать разработанные ими 
номограммы~\cite{Thomas1962}.  
Для использования номограмм необходимо было выполнить следующие операции.
Сначала производится измерение емкости при определенном напряжении смещения. 
Далее настройки измерительного моста изменяются, для чтобы его показания 
изменились на небольшую величину ($\sim$1\%). 
После чего прикладываемое смешение меняется так, чтобы сбалансировать 
измененную мостовую схему. 
Это позволяет определить  $\Delta C / \Delta V$ при заданных значениях $V$ 
(см. рис.~\ref{fig:nomograph}). 
Используя полученное значение $\Delta C / \Delta V$  
и зная площадь контакта, можно определить значение концентрации и ширины ООЗ. 
Концентрационный профиль можно построить по точкам, проводя измерения при 
разных смешениях. 

Использование такой методики позволяет избежать ошибок 
дифференцирования, связанных со слишком маленьким приращением емкости.
Другая интересная особенность подобных номограмм заключается в том, что в них 
наглядно показана связь между емкостью и шириной области объемного заряда, а 
также необходимость использования $C$ и $\Delta C/\Delta V$ для расчета 
концентрации.





\subsection{Метод обратной связи (метод Миллера)}
Другим методом измерения концентрации основанным на емкостных измерениях 
является 
метод обратной связи или метод Миллера.
Миллер (Miller) предложил этот метод в
1972 году~\cite{Miller1972}.
Метод обратной связи базируется на использовании
цепи обратной связи для управления величиной смещения границы обедненной 
области под действием переменного напряжения.
При небольшом увеличении напряжения смешения 
дополнительный заряд, накапливающийся на границе обеденной области, вызывает 
возрастание электрического поля

\begin{equation} \label{eq:miller}
\Delta E  = \frac{q}{\varepsilon\varepsilon_0}N\left(x\right) \Delta x,
\end{equation}
\noindent при этом, величина $\Delta E$ будет пропорциональна амплитуде 
переменного напряжения~$\Delta V$:
\begin{equation}
\Delta E = - \frac{\Delta V}{x}.
\end{equation}

Соотношение \eqref{eq:miller} лежит в основе метода обратной связи, и, если 
электрическое поле может быть изменено так, что смешение границы обедненной
области ($\Delta x$) будет оставаться постоянным, величина 
$N(x)$ будет пропорциональна $\Delta E$. 
И наоборот, если поддерживать 
постоянным $\Delta E$, то $\Delta x$ будет пропорционально $1/N(x)$.

\begin{figure}
     \includesvg[clean,width=0.9\textwidth]{images/methods_miller}
    \caption{Блок схема реализующая измерение концентрации по методу обратной 
    связи~\cite{Miller1972}.
    PSD~--- фазочувствительный детектор,
    Chopper~--- модулятор.
    \label{fig:methods_miller}}
\end{figure}


Блок схема реализующая метод Миллера в режима постоянного приращения 
напряженности поля ($\Delta E = \textrm{const.}$) представлена на 
рисунке~\ref{fig:methods_miller}.
На структуру подается постоянное напряжение $V_0$, а переменное напряжение с 
амплитудой $\Delta V$ меняется пропорционально величине $x$ за счет 
использования модулятора включенного в обратную связь со схемой измерения 
ширины ООЗ~---~$x$.
Промодулированное значение $\Delta x$ подается на фазочувствительный детектор 
(PSD) на выходе которого определяется величина $1/N(x)$.

Практически метод обратной связи имеет ряд преимуществ: возможность
измерения на образцах удаленных от измерительного прибора на большое 
расстояние, а также, при 
использовании этого метода, значительно упрощается, по сравнению с другими 
методами, калибровка измерительной схемы. 


\section{Нелинейные  емкостные методы измерения концентрации}

В предыдущих разделах рассматривались линейные эффекты, в которых
выходной сигнал будет иметь такую же частоту, что и сигнал подаваемый на 
структуру, но может отличатся по амплитуде и по фазе~\cite{Stroynov1991}. 
Однако, величина $dC/dV$ может быть измерена и по нелинейным эффектам, 
возникающим под воздействием синусоидального напряжения или 
тока~\cite{Berman1972}.


В общем случае, если к структуре приложено постоянное напряжение $u_{0}$  и 
переменное напряжение малой амплитуды $u_\sim(t)$, то мгновенное значение 
емкости будет иметь вид:

\begin{equation} \label{eq:cap_nonlin}
C(t)= C_{\,0} + \frac{dC}{dV} u_\sim + 
\frac{1}{2}\frac{d^2C}{dV^2}u_\sim^2+
\frac{1}{6}\frac{d^3C}{dV^3}u_\sim^3+
\dots
\end{equation}

Подставив выражение~\eqref{eq:cap_nonlin} в~\eqref{eq:cur_cap}, получим ток 
через 
структуру:
\begin{equation} \label{eq:met-capt}
i(t) = C(t)\frac{dV}{dt} = \left( C_{\,0} + \frac{dC}{dV} u_\sim + 
\frac{1}{2}\frac{d^2C}{dV^2}u_\sim^2+
\frac{1}{6}\frac{d^3C}{dV^3}u_\sim^3+
\dots\right)\frac{du_\sim}{dt}
\end{equation}

%%%%%%%%%%%%%%% Синусоидальное напряжение 
Нелинейные эффекты возникают и при подаче на структуру
 синусоидального напряжения.
Однако, использование такого сигнала для измерения  $dC/dV$ имеет ряд 
недостатков.
Рассмотрим случай синусоидального напряжения малой амплитуды: 
\begin{equation} \label{eq:met-sin}
u_\sim(t) = u_{1} \sin \omega_{1} t, \qquad  u_{1} \ll u_{0}.
\end{equation}
 

Подставляя \eqref{eq:met-sin}  в  \eqref{eq:met-capt} и ограничившись первыми 
ряда, получим:

\begin{equation} \label{eq:met-sin2}
\begin{split}
i(t) = \omega_1 C_{\,0}u_1\cos\omega_1t + \frac{1}{2} \frac{dC}{dV}\omega_1 
u_1^2 \cos 2\omega_1t+ \\ + \frac{1}{8} \frac{d^2C}{dV^2}\omega_1 u_1^3 
\left(\cos \omega_1t - \cos \omega_13t\right) + \dots
\end{split} 
\end{equation} 

Из выражения~\eqref{eq:met-sin2} следует, что при воздействии синусоидального 
напряжения на нелинейную емкость, возникают высшие гармоники тока. Амплитуда 
второй гармоники пропорциональна $dC/dV\!$. Аналогично, при протекании 
синусоидального тока через нелинейную емкость возникает вторая гармоника 
напряжения, пропорциональная  $dC/dV\!$.
Схема возникновения нелинейных эффектов при воздействии синусоидальным сигналом
приведена на рисунке~\ref{fig:methods_nonlinear_diag}.
%%%НЕДОСТАТОК:
При наличие второй гармоники на выходе генератора переменного 
напряжения или тока, искомый сигнал будет суммироваться с ней, 
что  приводит к большой погрешности при использовании 
синусоидального сигнала для измерения нелинейных эффектов.

\begin{figure}
    \includesvg[clean,width=0.7\textwidth]{images/methods_nonlinear_diag}
    \caption{Сравнение линейного и нелинейного отклика 
        системы при воздействии синусоидальным сигналом на систему с линейной и 
        нелинейной передаточной характеристикой.
%        ~\cite{Abaturov2000}.
        \label{fig:methods_nonlinear_diag}}
\end{figure}

%%%%%%%%%%%%%%%%%  Два синусоидльных напряжения
Использование двух синусоидальных напряжений позволяет увеличить точность 
измерения  производной $dC/dV\!$. 
Рассмотрим сигнал состоящий из суммы двух синусоидальных напряжений:
\begin{equation} \label{eq:met-sin3}
u_\sim(t) = u_1 \sin \left( \omega_{1} t + \theta \right) + u_2 \sin \omega_2t,
\end{equation}
\noindent таких, что $u_1 \ll u_{0}$ и $u_2 \ll u_{0}$. 
Если амплитуды $u_1$ и $u_2$ 
одного порядка, а частоты $\omega_1$ и $\omega_2$ также одного порядка и не 
кратны друг другу. Подставляя \eqref{eq:met-sin3} в  \eqref{eq:met-capt}, 
получим выражение для тока $i(t)$, содержащее, помимо частот $\omega_1$ и 
$\omega_2$, также гармоники $\omega_1+\omega_2$ и $\omega_1 -\omega_2$. 
Амплитуды последних гармоник имеют следующий вид:

\begin{equation} \label{eq:met-omegap}
I_{\omega_1+\omega_2} = \frac{1}{2}\frac{dC}{dV}u_1 u_2 (\omega_1+\omega_2) +
\frac{1}{16}\frac{d^3C}{dV^3}u_1 u_2 (u_1^2+u_2^2) (\omega_1+\omega_2)
\end{equation}

\begin{equation}  \label{eq:met-omegam}
I_{\omega_1-\omega_2} = \frac{1}{2}\frac{dC}{dV}u_1 u_2 (\omega_1-\omega_2) +
\frac{1}{16}\frac{d^3C}{dV^3}u_1 u_2 (u_1^2+u_2^2) (\omega_1-\omega_2)
\end{equation}

При малых амплитудах сигнала вторым слагаемым в выражениях  
\eqref{eq:met-omegap} и \eqref{eq:met-omegam} можно пренебречь. Тогда амплитуды 
токов на частотах $\omega_1+\omega_2$ и $\omega_1 -\omega_2$ будут 
пропорциональны $dC/dV\!$. Так как в этом методе отклик смещен по частоте 
относительно воздействия, то в нем  нет необходимости подавлять высшие 
гармоники входного сигнала.

%%%%%%%%%%%%%%%%%%% Два синусоидальных напряжения с частотанми, различающимеси 
%%%%%%%%%%%%%%%%%%%на несколько порядков

Если в сигнале~\eqref{eq:met-sin3} частоты отличаются, причем $\omega_2$ на 
несколько порядков меньше частоты $\omega_1$, то выражение для  тока 
на частоте $\omega_1$ примет следующий вид:

\begin{equation} \label{eq:met-iomega}
\begin{split}
i_{\omega_1}(t) = \omega_1 \Big [
C_{\,0} + \frac{dC}{dV}u_2 \sin \omega_2t + \frac{1}{8}\frac{d^2C}{dV^2}u_1^2 +
\frac{1}{4} \frac{d^2C}{dV^2}u_2^2  - \\ - \frac{1}{4}\frac{d^2C}{dV^2}u_2^2 
\cos 2\omega_2t \Big ]
u_1 \cos\left(\omega_1t + \theta\right)
\end{split}
\end{equation}
Из выражения~\eqref{eq:met-iomega} видно, что ток на частоте $\omega_1$ 
промодулирован частотами $\omega_2$ и $2\omega_2$. Если измерительная система 
построена так, что высшие гармоники $\omega_1$ не детектируются, то выражение 
для амплитуды тока на частоте $\omega_1$ упрощается:
\begin{equation}
I_{\omega_1} = \omega_1 u_1 \left[C_{\,0} + \frac{dC}{dV}u_2 \sin \omega_2 
t\right],
\end{equation}
\noindent таким образом, коэффициент модуляции тока на частоте $\omega_1$ будет 
пропорционален $dC/dV\!$. 



Рассмотренные варианты сигналов, которые можно использовать для измерения 
нелинейных эффектов, применяются в различных реализациях нелинейных методов.
Нелинейные методы независимо развивались как в электрохимии полупроводников для 
исследования процессов происходящих на интерфейсе полупроводник-электролит, так 
и применительно к диагностики полупроводниковых материалов и структур.
К основным нелинейным методам относятся: модуляционный метод, метод второй 
гармоники, метод амплитудной демодуляции, двухчастотный метод.

Первые попытки исследования нелинейного импеданса были тесно связанны с 
исследованием свойств электрохимических систем. Так, в 1961~г. были разработаны 
импульсные методы для исследования эффекта фарадеевского 
выпрямления~\cite{Delahay1961}. Однако, эти методы не позволяли получить 
достаточную точность измерения исследуемых эффектов, что дало толчок к 
разработке других нелинейных методов, позволяющих повысить чувствительность 
измерительной системы.
Далее приводится описание наиболее часто используемых нелинейных методов и 
рассматриваются их преимущества и недостатки.

\subsection{Модуляционный метод}

%\begin{figure}
%     \includegraphics[width=\textwidth]{methods_bcf}
%     \caption{Блок диаграмма прибора, реализующего модуляционный 
%              метод~\cite{Baxandall1971}.
%     \label{fig:methods_bcf}}
%\end{figure}

Одним из наиболее популярных нелинейных методов является модуляционный методов.
Именно использование этого метода позволило в 1974~г. создать первую 
автоматизированную установку для электрохимического вольт-фарадного 
профилирования.
%Блок схема этого метода представлена на рисунке~\ref{fig:methods_bcf}.
В отличие от дифференциального метода, модуляционный метод позволяет  
определить распределение концентрации из значений  $dC/dV$, полученных 
низкочастотной модуляцией высокочастотного сигнала. Сигналы, пропорциональные 
$C$ и $dC/dV$, выделяются фазочувствительным детектором и обрабатываются 
аналоговыми устройствами, так что на выходе создаются напряжения 
пропорциональные $x$ и $N(x)$. 
Califano и Luciano (1970) \cite{Califano1970} первыми предложили этот метод 
для расчета $dC/dV$. 
Однако приборная реализация этого метода, которая стала широко использоваться 
(напр. “JAC type 366”), была описана в работе  Baxandall и др. (1971) 
\cite{Baxandall1971}. 
 
%TODO про гармоники из Абатурова -- нужно разобраться
%На исследуемый образец одновременно подают переменное напряжение 
%~\SI{100}{\kilo\hertz} и~\SI{1}{\kilo\hertz}
%Возникает токовый отклик на боковых частотах ~\SI{99}{\kilo\hertz} 
%и~\SI{101}{\kilo\hertz}
%Емкость определяется по разности фаз на несущей частоте.


%TODO Phase sensitive detector -аналог lockIn Amp
%TODO нужно посмотреть принцып работы
%Для построения профиля концентрации на структуру подается напряжение смещения
 
Основное достоинство модуляционного метода состоит в слабом влиянии паразитных 
емкостей, что позволяет проводить измерения на образцах, находящихся на большом
расстоянии от измерительной схемы. Модуляционный метод позволяет преодолеть 
проблемы, связанные с малыми изменениями емкости при большой глубине области 
обеднения, но требует использования большой амплитуды 
(порядка~\SI{150}{\milli\volt}) высокочастотного возбуждающего сигнала. Это 
ограничивает разрешение по глубине, приводя к еще большему размытию 
концентрационного профиля, чем фундаментальное Дебаевское размытие. Большая 
амплитуда тестового сигнала также накладывает ограничение на минимальное 
напряжение смещения при котором производится измерение емкости из-за того, что 
генераторы сигнала, обычно, работают только при напряжениях одной полярности.
%Дополнительные трудности, так же, возникают из-за необходимости детектировать 
%боковые гармоники на фоне несущей частоты. 
 


%TODO имеет смысл добавить схему и описание к ней

%TODO подготовить рисунки объясняющие раздичие между линейными и  не линейными 
%методами

\subsection{Метод второй гармоники} \label{ch:methods:secondharm}
Другим распространенным нелинейным методом является метод второй гармоники или 
метод Коупланда (Copeland).
Метод основан на нелинейном взаимодействии внешнего переменного электрического
поля с носителями заряда для генерации напряжения на удвоенной частоте, из 
которого можно получить значения $x_d$ и $N(x_d)$. Mayer и Guldbrandsen 
\cite{Meyer1963}
предложили эту методику в 1963 году, а Copeland \cite{Copeland1969} в 1969 внес 
дополнения в этот метод.

Метод заключается в следующем. Через структуру с барьером Шоттки  
или \textit{p}-\textit{n} 
переходом пропускают небольшой переменный ток $I \cos \left(\omega t\right)$ 
высокой частоты~(порядка \SI{5}{\mega\hertz}), который создает осциллирующий 
пространственный заряд. Глубина обедненной области задается постоянным обратным 
смещением структуры. Напряжение на структуре $\Delta V$, 
обусловленное протеканием переменного тока, имеет составляющие на частоте тока 
и на удвоенной частоте, зависящее от толщины обедненного слоя и концентрации 
примеси.

При достаточно больших частотах вклад от емкости будет доминировать. 

%TODO из статьи Gupta привести вывод фоормулы

\begin{equation}
\label{eq:copeland_dv}
\Delta V = x_d \frac{I}{\varepsilon\varepsilon_0 q A} \cos \left(\omega 
t\right) + 
\frac{1}{N(x_d)}\frac{I^2}{4\varepsilon\varepsilon_0 q \omega^2 A^2}\left( 
\cos\left(2\omega t\right) + 1\right)
\end{equation}

\begin{figure}
    \includesvg[clean, width=0.6\textwidth]{images/SiC-2b_4_copl_1}
    \caption{Частотный спектр выходного сигнала в методе второй гармоники.    
    \label{fig:SiC-2b_4_copl_1}}
\end{figure}

Спектр выходного сигнала приведен на рисунке~\ref{fig:SiC-2b_4_copl_1}.
Выделяя амплитуды напряжения на структуре на частоте возбуждающего
сигнала и на удвоенной частоте, в процессе медленного изменения напряжения
смещения, можно построить концентрационный профиль.
Данный метод точнее дифференциального метода, так как исключает
необходимость вычисления угла наклона $dC/dV$ по экспериментальным данным
зависимости $C(V)$. Однако элегантная физическая формулировка этого метода
не так просто реализуема на практике.

Spiwak \cite{Spiwak1969} и Gupta \cite{Gupta1972} первыми реализовали этот 
метод в виде готового прибора. Они показали, что эта методика очень 
чувствительна к паразитным 
емкостям, из-за малых амплитуд второй гармоники. Поэтому исследуемая структура 
должна быть подключена напрямую к измерительному прибору.
Оборудование, применявшее метод с использованием второй гармоники
напряжения было вытеснено приборами, реализующими модуляционный метод.

%TODO перенести нижележащее в другой раздел
%\subsubsection{Выбор амплитуды и частоты тока}
%Амплитуды первой и второй гармоники ... зависят от площади контакта, частоты и 
%амплитуды тестового сигнала. 
%
%требования \cite{Gupta1972}:
%\begin{itemize}
%    \item Амплитуда первой гармоники не должна быть больше $kT/q$, т.е.
%    $I/\omega > kTC/q$
%    
%    \item Амплитуда второй гармоники должна быть больше значения $V_{2min}$, 
%    определяемого уровнем шумов прибора, т.е.
%    $I/\omega < 4q \varepsilon\varepsilon_0 A^2 N V_{2min}$
%    
%    \item Максимальная частота ограниченна ... сложность схемы
%    
%\end{itemize}



\subsection{Метод амплитудной демодуляции} \label{sec:adm}
\nomenclature{АДМ}{Метод амплитудной демодуляции}
В методе амплитудной демодуляции (АДМ) на структуру подается амплитудно 
модулированный высокочастотный сигнал (см. рис.~\ref{fig:methods_adm_signal}). 
Модуляция производится гармоническим сигналом низкой частоты. Эта же частота 
выделяется из отклика системы, то есть используется демодулированный сигнал.

%TODO добавить формулу для сигнала и рисунок
На структуру подается ток вида $I\left( 1 + \sin \left( \Omega t\right)  
\right) \sin \left( \omega t \right)  $, где $ \sin \left( \omega t \right)$ -- 
несущий высокочастотный сигал, а множитель $I\left( 1 + \sin \left( \Omega 
t\right)  \right)$ -- модулируемая амплитуда на частоте $\Omega$.
Частотный спектр сигнала приведен на рисунке~\ref{fig:SiC-2b_4_adml}.

\begin{figure}
\captionsetup[subfigure]{margin=12pt,captionskip=0pt}
    \xsubfloat{форма возбуждающего сигнала 
    }{\includesvg[clean,width=0.5\textwidth]{images/methods_adm_signal}
            \label{fig:methods_adm_signal}}
\captionsetup[subfigure]{margin=40pt,captionskip=-50pt}
     \xsubfloat{частотный спектр выходного сигнала}{
     \includesvg[clean, width=0.6\textwidth]{images/SiC-2b_4_adml}
      \label{fig:SiC-2b_4_adml}}
      \makexcaption[Метод амплитудной демодуляции:]
      \label{fig:methods_adm}
\end{figure}

Метод АДМ широко используется для анализа нелинейных характеристик 
электрохимических систем с металлическими и полупроводниковыми 
электродами~\cite{solomatin1977,Pleskov2002b}.
Однако, для нас особый интерес представляет применение 
этого метода для определения концентрации в полупроводниках. Здесь, так же как 
и в методе второй гармоники (см. раздел~\ref{ch:methods:secondharm}),
амплитуда выходного сигнала будет пропорциональна концентрации 
примеси~\cite{Abaturov2000}:

\begin{equation} \label{eq:adm_con}
V_{ADM} = \frac1{N_{D,A}}\frac{{I^2}}{2\varepsilon\varepsilon_0 q \omega^2 A^2},
\end{equation}
\noindent где $\omega$ -- несущая частота и $I$ -- амплитуда возбуждающего тока.

Преимуществом этого метода, по сравнению с методом второй гармоники, является 
то, что измеряемый отклик системы возникает на частоте много меньшей, чем 
несущая частота возбуждающего сигнала. Это позволяет использовать более простое 
оборудование для реализации метода.



\subsection{Двухчастотный метод}
Вариацией модуляционного метода, применяемого в электрохимии, является 
двухчастотный метод. Здесь, вместо модуляции напряжения производится модуляция 
тока.
В двухчастотном методе ток через структуру имеет вид: $I\left(\sin  \omega_a t  
+ m \sin \left( \omega_a  -  \omega_r\right) t  \right) $, при этом частоты 
$\omega_a$ (возбуждающая частота) и $\omega_r$ (модулирующая частота) 
выбираются таким образом, что $\omega_a  \gg  \omega_r$. Для отделения 
высокочастотной составляющей от измеряемого сигнала используется фильтр низких 
частот.

Если исследуемая структура ведет себя как нелинейная емкость, то амплитуда 
выходного напряжения на частоте $\omega_r$ будет иметь вид~\cite{Elkin1975}:

\begin{equation} \label{eq:elkin75}
V_r = \frac{{mI^2}}{2 \omega_a^2 A^2}\frac{dC/dV}{C^3}
\end{equation}
\noindent где $mI$ -- амплитуда возбуждающего тока на частоте 
$\left( \omega_a -  \omega_r\right) $.

%%TODO  по факту модуляционный метод и двухчастотный метод это одно и то же
%%TODO стой лишь разницей, что в первом на структуру подается напряжение
Способ воздействия током выгоден тем, что позволяет уйти от зависимости 
величины напряжения на ячейке от сопротивления электролита. Величина 
регистрируемого сигнала также не будет зависеть от сопротивления электролита, 
если выходное сопротивление генератора достаточно велико~ \cite{ambrozy1970}.


%\todo{Измеряя концентрацию при различных смещениях мы можем построить профиль 
%распределения концентрации свободных носителей заряда}
%
%\todo{Где-то должны быть оговорены тем приближения, которые использовались для 
%вывода формулы пересчета ВФХ в профиль концентрации. см. Батавин стр. 84-86}
