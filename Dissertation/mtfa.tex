\section{Расчет распределения концентрации основных носителей заряда с 
использованием модифицированного приближения Томаса-Ферми (MTFA)} \label{mtfa}
Модифицированное приближение Томаса-Ферми (modified Thomas-Fermi approximation 
или  MTFA) предложили G. Paasch и H. \"Ubensee в работе \cite{Paasch1982} как 
модификация квазиклассического приближения к определению локальной плотности 
состояний в полупроводниках. Это приближение позволяет ввести граничные условия 
для волновой функции на поверхности полупроводника, что актуально для структур 
с обратным изгибом зон на поверхности.

\nomenclature{MTFA}{Модифицированное приближение Томаса-Ферми}
%TODO почету так называется
%~\cite{demkov1965} Модель Томаса-Ферми для атома

\subsubsection{Локальная плотность состояний в объемных полупроводниках}
Наиболее простым приближением для расчета плотности состояний в 
многоэлектронной системе является приближение локальной плотности состояний.

Запишем уравнение Шредингера в приближении эффективной массы:
\begin{equation} \label{eq:schred} 
\hat{H}(\vb{r})\psi_i(\vb{r}) = E_i\psi_i(\vb{r}), \qquad
\hat{H}(\vb{r}) = - \frac{\hbar^2}{2m^*}
\frac{\partial^2 }{\partial \vb{r}^2} + V(\vb{r}),
\end{equation}
\noindent  где $\hat{H}$ --- гамильтониан системы и $V(\vb{r})$ --- эффективный 
одноэлектронный потенциал.


Рассмотрим матрицу плотности состояний \cite{kirzh2001, land2004b}, равную, с 
точностью до коэффициента, спектральной функции \cite{Datta2005} используемой в 
методе функции Грина:
\begin{equation} \label{eq:dos_matrix}
\rho (\vb{r}, \vb{r}'\!, E) =
2 \sum_{i}   \psi_i (\vb{r}) \delta (E - E_i) \psi_i^*(\vb{r}'),
\end{equation}
\noindent $\psi_i$ и $E_i$ --- собственные вектора и собственные значения 
гамильтониана $\hat{H}$ (\ref{eq:schred}).
Множитель 2 введен для учета спинового вырождения.

Волновые функции $\psi_i(\vb{r})$~--- ортонормированные и должны удовлетворять 
следующему соотношению:
\begin{equation} \label{eq:delta}
\sum_{i} \psi_i^* (\vb{r}' ) \psi_i (\vb{r}) = 
\delta (\vb{r} - \vb{r}' ).
\end{equation}

Тогда матрица плотности состояний (\ref{eq:dos_matrix}) с использованием 
уравнения 
Шредингера (\ref{eq:schred}) и соотношения (\ref{eq:delta}) может быть записана 
как:
\begin{align} \label{eq:dos_delta}
\nonumber 
\rho (\vb{r}, \vb{r}'\!, E) & = 
2 \sum_{i} \delta (E - H) \psi_i^* (\vb{r}' ) \psi_i (\vb{r}) \\ 
\nonumber
 & =  2 \, \delta (E - H) \sum_{i} \psi_i^* (\vb{r}' ) \psi_i (\vb{r})  \\ 
 & =  2 \, \delta (E - H) \,  \delta (\vb{r} - \vb{r}' ).
\end{align}


Воспользовавшись Фурье представлением $\delta$-функции
\begin{equation*}
\delta (\vb{r}- \vb{r}') = 
\frac1{{2\pi}^3} 
\int e^{i \vb{k} (\vb{r}- \vb{r}')}
\mathrm{d}\vb{k},
\end{equation*}

\noindent получим:
\begin{equation} \label{eq:lsd_integ0}
\rho (\vb{r}, \vb{r}'\!, E) =
\frac2{(2\pi)^3}
\int_{\Omega} \delta \left(E -
\left[\frac{\hbar^2}{2m^*} \frac{\partial^2 }{\partial \vb{r}^2} +
V(\vb{r}) \right]
\right)
e^{i \vb{k} (\vb{r}- \vb{r}')} 
\mathrm{d}^3\vb{k}.
\end{equation}

Полагая, что потенциал $V(\vb{r})$  изменяется медленно мы можем пренебречь его производными. 
В таком случае  $\partial^2 /\partial \vb{r}^2$ будет действовать только на экспоненту
\begin{equation} \label{eq:lsd_integ}
\rho (\vb{r}, \vb{r}'\!, E) =
\frac2{(2\pi)^3}
\int_{\Omega} \delta \left(E -
\left[\frac{\hbar^2}{2m^*} \vb{k}^2 + V(\vb{r}) \right]
\right)
e^{i \vb{k} (\vb{r}- \vb{r}')} 
\mathrm{d}^3\vb{k}.
\end{equation}

Для определения локальной плотности состояний нам нужны диагональные элементы матрицы плотности, т.е. значения интеграла (\ref{eq:lsd_integ}) только при $\vb{r}=\vb{r}'$. Вычисление этого интеграла даст стандартное выражение для локальной плотности состояний в объемном кристалле:
\begin{equation} \label{eq:lds}
\rho(\vb{r}, E) = 
\frac{4\pi}{(2\pi\hbar)^3}\left(2m^*\right)^{3/2}
\sqrt{E - V(\vb{r})}, \text{  при $E \geqslant V(\vb{r})$}.
\end{equation}

\subsubsection{Локальная плотность состояний вблизи поверхности}
Как отмечалось выше, выражение для локальной плотности состояний~\eqref{eq:lds} 
справедливо только при медленно изменяющемся потенциале $V(\vb{r})$. Однако 
вблизи поверхности полупроводника может происходить резкий скачок потенциала. 
Считая, что это скачек $\gg kT$, рассмотрим потенциал $V'(\vb{r})$ с разрывом в 
плоскости $z=0$: 
\begin{equation} \label{eq:potential}
V'(\vb{r}) = 
    \begin{cases}
    V(\vb{r}), & \text{при $z  > 0$} \\
    \rightarrow \infty,        & \text{при $z \leqslant 0$}.
    \end{cases}
\end{equation}

Потенциал (\ref{eq:potential}) приводит к появлению граничного условия для волновых функций:
\begin{equation} \label{eq:boundary}
\psi_i(\vb{r}) \bigg|_{z \leqslant 0}= 0.
\end{equation}

Дельта-функция от векторного аргумента ( трехмерная $\delta$--функция) определятся как произведение $\delta$--функций от каждой компоненты вектора  \cite{land2004b}:
\begin{equation*}
\delta (\vb{r} - \vb{r}' )  \rightarrow
\delta (x - x')\,\delta (y - y')\,\delta (z - z').
\end{equation*}

С учетом граничного условия (\ref{eq:boundary}) изменится соотношение 
(\ref{eq:delta}), определяющее нормировку собственных функций
\begin{equation} \label{eq:new_completeness}
\delta (\vb{r} - \vb{r}' )  \rightarrow
\delta (x - x')\,\delta (y - y')\,\left\{\delta (z - z') - \delta (z + 
z')\right\}.
\end{equation}

Используя (\ref{eq:new_completeness}) выражение (\ref{eq:lsd_integ}) для матрицы плотности состояний примет следующий вид:
\begin{align} \label{eq:new_matrix}
\nonumber
\rho (\vb{r}, \vb{r}'\!, E) & =
\frac2{(2\pi)^3}
\int_{\Omega} \delta \left(E -
\left[\frac{-\hbar^2}{2m^*}\frac{\partial^2 }{\partial \vb{r}^2}  +
V(\vb{r}) \right]
\right)
e^{i \left[ k_x \delta (x - x') + k_y \delta (y - y')\right] } \times \\
& \times 
\left(e^{i k_z (z - z')} - e^{i k_z (z + z')}\right)
\mathrm{d}^3\vb{k}.
\end{align}

Как и ранее, считая что потенциал $V(\vb{r})$ меняется медленно и оператор 
кинетической энергии действует только на экспоненциальную функцию, можно 
заменить $\partial^2 /\partial \vb{r}^2$ на $\hbar^2\vb{q}^2/2m$.
Тогда, для $\vb{r} = \vb{r}'$, получим:

\begin{equation} \label{eq:mlds}
\rho(\vb{r}, E) = \frac{m}{\pi^2\hbar^2} K(E, V(\vb{r})) 
\left[1 - \text{J}_0 \Big( 2zK(E, V(\vb{r})) \Big)  \right],
\end{equation}
\noindent где
\begin{equation}
K(E, V(\vb{r})) = \left(\frac{2m}{\hbar^2}\left(E - V(\vb{r}) \right)  \right) 
^{1/2}
\end{equation}
\noindent и $\text{J}_0$~--- сферическая функция Бесселя нулевого порядка.
Это приближение назвается модифицированным приближением локальной плотности или 
модифицированным приближением Томаса-Ферми.

Сравним рассмотренные выше модели на примере треугольной потенциальной 
ямы с линейно изменяющимся потенциалом и бесконечным барьером на одной из 
грани:

\begin{equation}
V(r) = \left\{\begin{matrix}
q Fz &  z >0 &\\ 
\infty &  z\leqslant0~, & 
\end{matrix}\right.
\end{equation} 
\noindent где $F$~--- напряженность электрического поля на поверхности.
Этот пример интересен, так как,  вблизи поверхности или границы раздела в 
полупроводнике потенциал можно, зачастую, аппроксимировать линейной функцией.

Точное решение этой задачи хорошо известно (см. 
например~\cite{demihovski2000}).  Уравнение Шредингера для такой  системы может 
быть преобразовано к дифференциальному уравнению для функций Эйри, тогда 
собственные функции будут иметь вид:

\begin{equation}
\psi_n(z) = c_n \, \text{Ai}\left(  \left(\frac{2 m q F}{\hbar} \right)^{1/3}
\left(z - \frac{E_n}{q F} \right) 
 \right),
\end{equation}
\noindent где $c_n$~--- нормированная константа, $E_n$~--- дискретные 
энергические уровни в треугольной яме, которые определяются из корней функции 
Эйри $a_n~(n = 1,2,\dots)$

\begin{equation}
E_n = \left(\frac{q^2F^2\hbar^2}{2 m} \right)^{1/3}.
\end{equation}

Локальная плотность состояний определяется из выражения~\eqref{eq:dos_matrix}  
и в такой системе будет равна

\begin{equation} \label{eq:ld_exact}
\rho(z, E) = \frac{m}{\pi\hbar^2}\sum_n \theta \left( E - E_n\right) 
\psi_n^2(z),
\end{equation}
\noindent где $\theta$~--- функция Хевисайда.


\begin{figure}
\newcommand{\LDA}{LDA}
\newcommand{\modified}{MLDA}
\newcommand{\Exact}{exact}
\includesvg[clean, width=0.7\textwidth]{images/IP_1982_DOS}
\caption{Зависимость локальной плотности состояний от координаты для трех 
уровней в треугольной яме \textit{E}$_\textrm{1}$, 
\textit{E}$_\textrm{5}$, \textit{E}$_\textrm{15}$.
\label{fig:IP_1982_DOS}
}
\end{figure}

Зависимость локальной плотности состояний от координаты для трех разных 
собственных значений, рассчитанная в приближении локальной 
плотности~\eqref{eq:lds} и модифицированном приближении  локальной 
плотности~\eqref{eq:mlds} в сравнении с точным решением~\eqref{eq:ld_exact} 
приведено на рисунке~\ref{fig:IP_1982_DOS}.
Расчет выполнен для напряженности электрического поля 
равной~\SI{1}{\mega\volt/\metre}.
Локальная плотность состояний, рассчитанная с использованием модифицированного 
приближения локальной плотности, точнее совпадает с точным решением вблизи 
поверхности по сравнению с результатами расчета в обычном приближении.


\begin{figure}
\newcommand{\LDA}{LDA}
\newcommand{\modified}{MLDA}
\newcommand{\Exact}{exact}
\captionsetup[subfigure]{margin=43pt,captionskip=-37pt}
\xsubfloat{\textit{z}~=~1~нм}{
    \includesvg[clean, width=0.6\textwidth]{images/IP_1982_DOS_E_1}
    \label{fig:IP_1982_DOS_E_1}}
\xsubfloat{\textit{z}~=~2~нм}{
    \includesvg[clean, width=0.6\textwidth]{images/IP_1982_DOS_E_2}
    \label{fig:IP_1982_DOS_E_2}}
\xsubfloat{\textit{z}~=~6~нм}{
    \includesvg[clean, width=0.6\textwidth]{images/IP_1982_DOS_E_3}
    \label{fig:IP_1982_DOS_E_3}}
\vspace{1em}
\makexcaption[Зависимость локальной функции состояний от энергии для:]
\label{fig:IP_1982_DOS_E}
\end{figure}

Более детально различия между рассмотренными приближениями и точным решением 
видны на рисунке~\ref{fig:IP_1982_DOS_E}. Здесь зависимость локальной плотности 
от энергии вблизи поверхности, рассчитанная в приближении локальной плотности,
сильно отличается как от модифицированного приближения, так и от точного 
решения. Однако, по мере удаления от поверхности разница между приближения 
становится незначительной.

\begin{figure}
\newcommand{\LDA}{LDA}
\newcommand{\modified}{MLDA}
\newcommand{\Exact}{exact}
\includesvg[clean, width=0.6\textwidth]{images/IP_1982_comp}
\caption{Распределение концентрации электронов внутри треугольной потенциальной 
ямы рассчитанная с использованием трех моделей:
LDA --- приближение локальной плотности,
MLDA --- модифицированное приближение локальной плотности (модифицированное 
приближение Томаса-Ферми),
exact --- аналитическое решение.
\label{fig:IP_1982_comp}
}
\end{figure}

Сравнение результатов расчета распределения электронов с учетом трех моделей 
приведено на рисунке~\ref{fig:IP_1982_comp}.
Точное решение и модифицированное приближение практически совпадают для тех 
параметров треугольной ямы, которая использовалась в расчетах.
Использование приближения локальной плотности приводит к неверному значению 
концентрации вблизи поверхности, но дает правильное распределение на глубине 
более~\SI{5}{\nm} от поверхности. 

\begin{samepage}
Концентрация электронов получена из локальной плотности состояний с 
использованием следующего выражения:

\begin{equation}
n(\vb{r}) = \int_{V(\vb{r})}^{\infty} \rho(\vb{r}, E) f(E)\,dE,
\end{equation} 
\noindent где $\rho(\vb{r}, E)$~--- локальная плотность состояний для каждой из 
рассмотренных моделей и 
$f(E)$~--- функция Ферми-Дирака

\begin{equation}
f(E) = \frac{1}{1+ \exp\left(\frac{E-E_|F|}{kT}\right)}.
\end{equation}
\end{samepage}

Особенности применения модифицированного приближения локальной плотности 
(модифицированное приближения Томаса-Ферми, MTFA)  в полупроводниковых 
материала будут рассмотрены с следующем разделе.
