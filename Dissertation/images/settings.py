import numpy as np
from numpy import sinc, inf, zeros_like, zeros, ones_like, ones
import locale
locale.setlocale(locale.LC_ALL, 'us_US')
# locale.setlocale(locale.LC_ALL, 'deu_deu')
import matplotlib
matplotlib.rcParams['axes.formatter.use_locale'] = True
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.ticker import Formatter, decimal
import math
import ecvproc
import admproc
import mtfa
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from numpy import pi
from itertools import zip_longest

#font_family = 'serif'
font_family = 'sans'
font_size = 18
params= {'text.usetex': False,
#         'mathtext.fontset': 'stix',
         'mathtext.fontset': 'stixsans',
         'font.size': font_size,
         'axes.labelsize': font_size,
         'font.family': font_family,
         'figure.figsize': [6, 4.8],
         'legend.fontsize': font_size,
         'svg.fonttype':'none'
         }
plt.rcParams.update(params)

from matplotlib.ticker import FuncFormatter

def format_exp(x, pos):
    if x == 0:
        return r'0'
    if x == 1:
        return r'\$10^0$'
    else:
        return '\\num{e%.0f}' %np.log10(x)
exponent = FuncFormatter(format_exp)

def sci_notation(num, decimal_digits=1, precision=None, exponent=None):
    if not exponent: 
        exponent = int(math.floor(math.log10(abs(num))))
    coeff = round(num/float(10**exponent), decimal_digits)
    if not precision:
        precision = decimal_digits
    
    return "{0:.{2}f} \\times 10^{{{1:d}}}".format(coeff, exponent, 
                                                     precision)

def color_variant(hex_color, brightness_offset=1):
    """ takes a color and produces a lighter or darker varian of it"""
    if len(hex_color) != 7:
        raise Exception('Passed {} into color_variant(), needs to \
                         be in #87c95f format.'.format(hex_color))

    rgb_hex = [hex_color[x:x+2] for x in [1,3,5]]
    new_rgb_int = [int(hex_value, 16) + brightness_offset for
                   hex_value in rgb_hex]
    new_rgb_int = [min([255, max([0, i])]) for i in new_rgb_int]
    return '#' + ''.join(['{:02x}'.format(i) for i in new_rgb_int])
    
class Units:
    def __init__(self):
        global si;
        si = {
              -18 : {'multiplier' : 10 ** 18, 'prefix' : r'\atto'},
              -17 : {'multiplier' : 10 ** 18, 'prefix' : r'\atto'},
              -16 : {'multiplier' : 10 ** 18, 'prefix' : r'\atto'},
              -15 : {'multiplier' : 10 ** 15, 'prefix' : r'\femto'},
              -14 : {'multiplier' : 10 ** 15, 'prefix' : r'\femto'},
              -13 : {'multiplier' : 10 ** 15, 'prefix' : r'\femto'},
              -12 : {'multiplier' : 10 ** 12, 'prefix' : r'\pico'},
              -11 : {'multiplier' : 10 ** 12, 'prefix' : r'\pico'},
              -10 : {'multiplier' : 10 ** 12, 'prefix' : r'\pico'},
              -9 : {'multiplier' : 10 ** 9, 'prefix' : r'\nano'},
              -8 : {'multiplier' : 10 ** 9, 'prefix' : r'\nano'},
              -7 : {'multiplier' : 10 ** 9, 'prefix' : r'\nano'},
              -6 : {'multiplier' : 10 ** 6, 'prefix' : r'\micro'},
              -5 : {'multiplier' : 10 ** 6, 'prefix' : r'\micro'},
              -4 : {'multiplier' : 10 ** 6, 'prefix' : r'\micro'},
              -3 : {'multiplier' : 10 ** 3, 'prefix' : r'\milli'},
              -2 : {'multiplier' : 10 ** 2, 'prefix' : r'\centi'},
              -1 : {'multiplier' : 10 ** 1, 'prefix' : r'\deci'},
               0 : {'multiplier' : 1, 'prefix' : ''},
               1 : {'multiplier' : 1, 'prefix' : ''},
               2 : {'multiplier' : 1, 'prefix' : ''},
               3 : {'multiplier' : 10 ** 3, 'prefix' : r'\kilo'},
               4 : {'multiplier' : 10 ** 3, 'prefix' : r'\kilo'},
               5 : {'multiplier' : 10 ** 3, 'prefix' : r'\kilo'},
               6 : {'multiplier' : 10 ** 6, 'prefix' : r'\mega'},
               7 : {'multiplier' : 10 ** 6, 'prefix' : r'\mega'},
               8 : {'multiplier' : 10 ** 6, 'prefix' : r'\mega'},
               9 : {'multiplier' : 10 ** 9, 'prefix' : r'\giga'},
              10 : {'multiplier' : 10 ** 9, 'prefix' : r'\giga'},
              11 : {'multiplier' : 10 ** 9, 'prefix' : r'\giga'},
              12 : {'multiplier' : 10 ** 12, 'prefix' : r'\terra'},
              13 : {'multiplier' : 10 ** 12, 'prefix' : r'\terra'},
              14 : {'multiplier' : 10 ** 12, 'prefix' : r'\terra'},
              }

    def convert(self, number):
        # Checking if its negative or positive
        if number < 0:
            negative = True;
        else:
            negative = False;

        # if its negative converting to positive (math.log()....)
        if negative:
            number = number - (number*2);

        # Taking the exponent
        exponent = int(math.log10(number));

        # Checking if it was negative converting it back to negative
        if negative:
            number = number - (number*2);

        # If the exponent is smaler than 0 dividing the exponent with -1
        if exponent < 0:
            exponent = exponent-1;
            return [number * si[exponent]['multiplier'], si[exponent]['prefix']]; 
        # If the exponent bigger than 0 just return it
        elif exponent > 0:
            return [number / si[exponent]['multiplier'], si[exponent]['prefix']]; 
        # If the exponent is 0 than return only the value
        elif exponent == 0:
            return [number, ''];

units = Units()
def format_si(num, pos=1):
    rValue, rPrefix = units.convert(num)
    return r'\SI{'+'{:.0f}'.format(rValue)+'}{'+'{:s}'.format(rPrefix)
# siunit = FuncFormatter(format_si)

class EngLatexFormatter(Formatter):
    """
    Formats axis values using engineering prefixes to represent powers of 1000,
    plus a specified unit, e.g., 10 MHz instead of 1e7.
    """

    # the unicode for -6 is the greek letter mu
    # commeted here due to bug in pep8
    # (https://github.com/jcrocholl/pep8/issues/271)

    # The SI engineering prefixes
    ENG_PREFIXES = {
        -18: r'}{\atto',
        -15: r'}{\femto',
        -12: r'}{\pico',
         -9: r'}{\nano',
         -6: r'}{\micro',
         -3: r'}{\milli',
          0: "}{",
          3: r'}{\kilo',
          6: r'}{\mega',
          9: r'}{\giga',
         12: r'}{\terra',
         15: "P",
         18: "E",
         21: "Z",
         24: "Y"
    }

    def __init__(self, unit="", places=None):
        self.unit = unit+'}'
        self.places = places

    def __call__(self, x, pos=None):
        s = "%s%s" % (self.format_eng(x), self.unit)
        return self.fix_minus(s)

    def format_eng(self, num):
        """ Formats a number in engineering notation, appending a letter
        representing the power of 1000 of the original number. Some examples:

        >>> format_eng(0)       # for self.places = 0
        '0'

        >>> format_eng(1000000) # for self.places = 1
        '1.0 M'

        >>> format_eng("-1e-6") # for self.places = 2
        u'-1.00 \u03bc'

        @param num: the value to represent
        @type num: either a numeric value or a string that can be converted to
                   a numeric value (as per decimal.Decimal constructor)

        @return: engineering formatted string
        """

        dnum = decimal.Decimal(str(num))

        sign = 1

        if dnum < 0:
            sign = -1
            dnum = -dnum

        if dnum != 0:
            pow10 = decimal.Decimal(int(math.floor(dnum.log10() / 3) * 3))
        else:
            pow10 = decimal.Decimal(0)

        pow10 = pow10.min(max(self.ENG_PREFIXES.keys()))
        pow10 = pow10.max(min(self.ENG_PREFIXES.keys()))

        prefix = self.ENG_PREFIXES[int(pow10)]

        mant = sign * dnum / (10 ** pow10)

        if self.places is None:
            format_str = "%g %s"
        elif self.places == 0:
            format_str = "%i %s"
        elif self.places > 0:
            format_str = ("%%.%if %%s" % self.places)

        formatted = format_str % (mant, prefix)

        return r'\SI{'+formatted.strip()

siunit = EngLatexFormatter(unit=r'\ ', places=0)